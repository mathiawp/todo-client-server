// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { Card, Row, Column, TaskItem, Form, Button } from './widgets';
import taskService, { type Task } from './task-service';

class TaskList extends Component {
  tasks: Task[] = [];

  render() {
    return (
      <Card title="Tasks">
        <Row key={'init-boy'}>
          <Column>
            <h4>Title</h4>
          </Column>
          <Column >
            <h4 className="text-center">Done</h4>
          </Column>
          <Column>
            <h4>Remove</h4>
          </Column>
        </Row>
        {this.tasks.map((task) => (
          <Row key={task.id}>
            <Column>
              <TaskItem
                key={task.id}
                task={task}
                toggleCompletion={() => {
                  taskService.toggleTaskCompletion(task.id).then((data) => {
                    TaskList.instance()?.mounted();
                  })
                }}
                removeItem={() => {
                  taskService.delete(task.id).then((data) => {
                    TaskList.instance()?.mounted();
                  })
                }}
              />
            </Column>
          </Row>
        ))}
      </Card>
    );
  }

  mounted() {
    taskService.getAll().then((tasks) => (this.tasks = tasks))
  }
}

class TaskNew extends Component {
  title = '';

  render() {
    return (
      <Card title="New task">
        <Row>
          <Column width={1}>
            <Form.Label>Title:</Form.Label>
          </Column>
          <Column width={4}>
            <Form.Input
              type="text"
              value={this.title}
              onChange={(event) => (this.title = event.currentTarget.value)}
            />
          </Column>
        </Row>
        <Button.Success
          onClick={() => {
            taskService.create(this.title).then(() => {
              // Reloads the tasks in the Tasks component
              TaskList.instance()?.mounted(); // .? meaning: call TaskList.instance().mounted() if TaskList.instance() does not return null
              this.title = '';
            });
          }}
        >
          Create
        </Button.Success>
      </Card>
    );
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <>
      <TaskList />
      <TaskNew />
    </>,
    root
  );
